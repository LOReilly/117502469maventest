/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simonwoodworth.mavenwebapp;

import org.junit.Test;
import static org.junit.Assert.*;


/**
 *
 * @author simon
 */
public class CalculatorTest {
    
    //Some simple tests.
    
    @Test
    public void testAddThree() {
        assertEquals(6.0,Calculator.addThree(1.0, 2.0, 3.0), 0.0);
    }
    
    @Test
    public void testMultiplyThree() {
        assertEquals(6.0,Calculator.multiplyThree(1.0, 2.0, 3.0), 0.0);
    }
    
    @Test
    public void testMaxThree() {
        assertEquals(3.0,Calculator.maxThree(1.0, 2.0, 3.0), 0.0);
    }
    
    @Test
    public void testMinThree() {
        assertEquals(1.0,Calculator.minThree(1.0, 2.0, 3.0), 0.0);
    }
    
     @Test
    public void testAvgThree() {
        assertEquals(2.0,Calculator.avgThree(1.0, 2.0, 3.0), 0.0);
    }
    
}
